# snap

A cli implementation of the classic game card game snap

# Technical choices

* Java17

* Maven

* Spring



# Decisions

* By using spring we can make use of it's in memory message bus as well as IOC.


# Delivery milestones:
After each milestone we should have a feature improvement that can be tested by customers.

1.  allow for 2 players to be able to play a game of snap.
    - Ask for the players names to be entered at the start of the game.  - Done
    - A single deck of cards exists which is shuffled and then dealt to the players. - Done
    - When two cards don't match - Return is selected to apply the next pile. - Done
    - When two cards match - the name is entered for who won the round. - TODO
    - When a player has been assigned snap then all the cards in the centre pile
      should be assigned to the players winning pot. - TODO
    - When the initial distribution of cards has been exhausted then assign the
      winner based on the size of the winning pot. - TODO
    - When there is no winner then a different error message is shown - Done
2.  allow for 2 players to choose the game mode i.e suit only, face only or both
    - Ask the players what mode of the game they would like to play and extend the matching rules based on the input.
3.  allow for 2 players to choose how many decks they'd like to use
    - Ask the players how many decks they would like to use and extend the distribution to handle the number of decks.
4. allow for 2 players to choose how the game ends, e.g when all cards are on a single stack or when the deck has been exhausted.
    - Ask the players how they would like to end the game and then apply the end rule to the rest of the game
5. allow for any number of players to join the system.
    - Ask for how many players they would like to join the game and then ask for their names.

# How to run
- Make sure that you have the Java17 setup and running
- ./mvnw spring-boot:run