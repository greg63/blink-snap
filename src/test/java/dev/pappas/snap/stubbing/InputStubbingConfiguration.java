package dev.pappas.snap.stubbing;

import dev.pappas.snap.utils.InputListener;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestConfiguration
public class InputStubbingConfiguration {

    @Bean
    @Primary
    public InputListener inputListener() {
        InputListener inputListener = mock(InputListener.class);
        when(inputListener.getNextString()).thenReturn("Alex", "Sam");
        return inputListener;
    }
}
