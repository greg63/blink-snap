package dev.pappas.snap.deck.service;

import dev.pappas.snap.deck.api.Card;
import dev.pappas.snap.deck.api.Face;
import dev.pappas.snap.deck.api.NoCardsLeftException;
import dev.pappas.snap.deck.api.Suit;
import dev.pappas.snap.deck.domain.CardEntity;
import dev.pappas.snap.deck.repository.DeckRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Stack;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DeckServiceTest {

    @Mock
    DeckRepository deckRepository;

    @Captor
    ArgumentCaptor<Stack<CardEntity>> cardDeckCaptor;

    @Test
    @DisplayName("when the createDeck function is invoked then a deck of 52 cards is invoked")
    public void testDeckCreatesAFullDeck() {
        //setup
        DeckService deckService = new DeckService(deckRepository);
        //act
        deckService.createDeck();
        //verify
        verify(deckRepository).createDeck(cardDeckCaptor.capture());
        assertThat(cardDeckCaptor.getValue().size()).isEqualTo(52);
    }

    @Test
    @DisplayName("when the createDeck function is invoked multiple times then the resulting stacks are not equal")
    public void testForRandom() {
        //setup
        DeckService deckService = new DeckService(deckRepository);
        //act
        deckService.createDeck();
        deckService.createDeck();
        deckService.createDeck();
        //verify
        verify(deckRepository, times(3)).createDeck(cardDeckCaptor.capture());

        List<Stack<CardEntity>> cards = cardDeckCaptor.getAllValues();

        assertThat(cards.get(0)).isNotEqualTo(cards.get(1)).isNotEqualTo(cards.get(2));
    }

    @Test
    @DisplayName("test that fetchNextCard Will Return A Card from the top of the stack")
    public void testCardReturnedFromTopOfStack() throws NoCardsLeftException {
        //setup
        CardEntity cardEntityOne = new CardEntity(Face.THREE, Suit.HEARTS);
        CardEntity cardEntityTwo = new CardEntity(Face.FOUR, Suit.DIAMONDS);
        Stack<CardEntity> cardEntities = new Stack<>();
        cardEntities.push(cardEntityOne);
        cardEntities.push(cardEntityTwo);

        DeckRepository deckRepository = new DeckRepository();
        deckRepository.createDeck(cardEntities);
        DeckService deckService = new DeckService(deckRepository);
        //act
        Card card = deckService.getNextPlayerCard();
        //verify
        assertThat(card.face()).isEqualTo(cardEntityTwo.getFace());
        assertThat(card.suit()).isEqualTo(cardEntityTwo.getSuit());
    }

    @Test
    @DisplayName("test that when fetchNextCard is called and there isn't one then a NoCardsLeftException is thrown")
    public void testThrows() {
        //setup
        DeckRepository deckRepository = new DeckRepository();
        DeckService deckService = new DeckService(deckRepository);
        deckRepository.createDeck(new Stack<>());
        //act
        assertThatThrownBy(() -> deckService.getNextPlayerCard())
                //verify
                .isInstanceOf(NoCardsLeftException.class);

    }

}