package dev.pappas.snap;

import dev.pappas.snap.stubbing.InputStubbingConfiguration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@Import(InputStubbingConfiguration.class)
@TestPropertySource(properties = "spring.main.allow-bean-definition-overriding=true")
class SnapApplicationTest {

    @Test
    @DisplayName("test that the application starts")
    public void testThatTheApplicationStarts() {

    }

}