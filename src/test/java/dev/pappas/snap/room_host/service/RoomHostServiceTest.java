package dev.pappas.snap.room_host.service;

import dev.pappas.snap.deck.api.Card;
import dev.pappas.snap.deck.api.Face;
import dev.pappas.snap.deck.api.NoCardsLeftException;
import dev.pappas.snap.deck.api.Suit;
import dev.pappas.snap.deck.service.DeckService;
import dev.pappas.snap.player.api.Player;
import dev.pappas.snap.player.service.PlayerService;
import dev.pappas.snap.room_host.api.StartTurn;
import dev.pappas.snap.room_host.domain.GameStateEntity;
import dev.pappas.snap.room_host.repository.GameStateRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoomHostServiceTest {

    private GameStateRepository gameStateRepository = new GameStateRepository();

    @Mock
    private PlayerService profileService;

    @Mock
    private DeckService deckService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    @DisplayName("test that when roomHostService is invoked to create a GameStateEntity is created and persisted" +
            "Test that the Deck service is invoked to create a deck, that the players are created and the first cards are " +
            "assigned for the first turn in the game")
    public void testGameStarted() throws NoCardsLeftException {
        //setup
        RoomHostService roomHostService = new RoomHostService(gameStateRepository, deckService, profileService, applicationEventPublisher);
        List<String> playerNames = List.of("Alex", "Sam");
        when(profileService.createPlayers(playerNames)).thenReturn(List.of(new Player("Alex", 0), new Player("Sam", 0)));
        when(deckService.getNextPlayerCard()).thenReturn(new Card(Face.ACE, Suit.CLUBS),
                new Card(Face.THREE, Suit.HEARTS));
        //act
        roomHostService.launchGame(playerNames);
        //verify
        GameStateEntity expectedState = new GameStateEntity();
        expectedState.addToPot(2);
        assertThat(gameStateRepository.fetch()).isEqualTo(expectedState);
        verify(profileService).createPlayers(anyList());
        verify(deckService).createDeck();
        verify(deckService, times(2)).getNextPlayerCard();
        verify(applicationEventPublisher).publishEvent(any(StartTurn.class));
    }

    @Test
    @DisplayName("test that when a completed turn is triggered but no name is entered then the next turn is retriggered")
    public void testTurnCompletion() throws NoCardsLeftException {
        //setup
        RoomHostService roomHostService = new RoomHostService(gameStateRepository, deckService, profileService, applicationEventPublisher);
        List<Player> players = List.of(new Player("Alex", 0), new Player("Sam", 0));
        when(profileService.fetchPlayers()).thenReturn(players);
        when(deckService.getNextPlayerCard()).thenReturn(new Card(Face.ACE, Suit.CLUBS),
                new Card(Face.THREE, Suit.HEARTS));
        gameStateRepository.save(new GameStateEntity());
        //act
        roomHostService.completeTurn("");
        //verify
        GameStateEntity expectedGameState = new GameStateEntity();
        expectedGameState.addToPot(2);

        assertThat(gameStateRepository.fetch()).isEqualTo(expectedGameState);
        verify(deckService, times(2)).getNextPlayerCard();
        verify(applicationEventPublisher).publishEvent(any(StartTurn.class));
    }



}