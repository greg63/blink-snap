package dev.pappas.snap.room_host.adapter;

import dev.pappas.snap.deck.api.Card;
import dev.pappas.snap.deck.api.Face;
import dev.pappas.snap.deck.api.Suit;
import dev.pappas.snap.player.api.Player;
import dev.pappas.snap.room_host.api.PlayersTurn;
import dev.pappas.snap.room_host.api.EndGame;
import dev.pappas.snap.room_host.api.StartTurn;
import dev.pappas.snap.room_host.service.RoomHostService;
import dev.pappas.snap.stubbing.InputStubbingConfiguration;
import org.junit.jupiter.api.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;

import java.io.PrintStream;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;


@SpringBootTest
@Import(InputStubbingConfiguration.class)
@TestPropertySource(properties = {
        "spring.main.allow-bean-definition-overriding=true",
        "dev.pappas.snap.multi_threaded.enabled=false"
})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CLIAdapterTest {

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    CLIAdapter cliAdapter;

    @MockBean
    PrintStream printStream;

    @MockBean
    RoomHostService roomHostService;

    @Captor
    ArgumentCaptor<List<String>> namesCaptor;

    @Captor
    ArgumentCaptor<String> outputCapture;

    @Test
    @DisplayName("test that the names are collected and passed to the roomHostService")
    @Order(0)
    public void testRoomHostServiceCollectsTheNamesOnGameStart() {
        //act
        //verify
        verify(roomHostService).launchGame(namesCaptor.capture());
        assertThat(namesCaptor.getValue()).asList()
                .contains("Sam")
                .contains("Alex");

    }

    @Test
    @DisplayName("test when the turn is called then the correct player information is displayed and the input is collected")
    @Order(10)
    public void testTurnIsCalled() {
        //setup
        int stake = 8;
        //act
        applicationEventPublisher.publishEvent(StartTurn.builder()
                .source(this)
                .playerOne(new PlayersTurn("Sam", new Card(Face.THREE, Suit.HEARTS)))
                .playerTwo(new PlayersTurn("Alex", new Card(Face.THREE, Suit.CLUBS)))
                .stake(stake)
                .build());
        //verify

        verify(printStream, atLeastOnce()).println(outputCapture.capture());

        assertThat(outputCapture.getAllValues().stream().filter(it -> it.startsWith("Current stake size is")).findFirst().get())
                .contains("" + stake);

        assertThat(outputCapture.getAllValues()).asList().contains("Sam plays: THREE of HEARTS");
        assertThat(outputCapture.getAllValues()).asList().contains("Alex plays: THREE of CLUBS");
        verify(roomHostService).completeTurn(anyString());
    }

    @Test
    @DisplayName("test that when end game is called then the correct player information is displayer in the correct input")
    @Order(20)
    public void testEndGame() {
        //act
        applicationEventPublisher.publishEvent(EndGame.builder()
                .source(this)
                .hasWinner(true)
                .winner(new Player("Sam", 12))
                .loser(new Player("Alex", 4))
                .build());
        //verify
        verify(printStream, atLeastOnce()).println(outputCapture.capture());

        assertThat(outputCapture.getAllValues().stream().filter(it -> it.startsWith("And the winner is...")).findFirst().get())
                .contains("Sam");

        assertThat(outputCapture.getAllValues().stream().filter(it -> it.startsWith("And the loser is...")).findFirst().get())
                .contains("Alex");
    }

    @Test
    @DisplayName("test that when nobody wins the result is also called")
    @Order(30)
    public void testEndGameNoWinner() {
        //act
        applicationEventPublisher.publishEvent(EndGame.builder()
                .source(this)
                .hasWinner(false)
                .winner(new Player("Sam", 12))
                .loser(new Player("Alex", 4))
                .build());
        //verify
        verify(printStream, atLeastOnce()).println(outputCapture.capture());
        assertThat(outputCapture.getAllValues().stream().anyMatch(it -> it.equals("nobody won!"))).isTrue();
    }
}