package dev.pappas.snap.player.service;

import dev.pappas.snap.player.api.Player;
import dev.pappas.snap.player.repository.PlayerRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
class PlayerServiceTest {

    private PlayerRepository playerRepository = new PlayerRepository();

    @DisplayName("when create players is invoked then two players are created with the names passed into the event")
    @Test
    public void testPlayerCreation() {
        //setup
        PlayerService playerService = new PlayerService(playerRepository);
        //act
        List<Player> players = playerService.createPlayers(List.of("Alex", "Sam"));
        //verify
        assertThat(players.get(0).name()).isEqualTo("Alex");
        assertThat(players.get(1).name()).isEqualTo("Sam");
    }
}