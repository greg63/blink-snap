package dev.pappas.snap.deck.api;

public enum Suit {
    HEARTS, SPADES, DIAMONDS, CLUBS
}

