package dev.pappas.snap.deck.api;

public record Card(Face face, Suit suit) {
    @Override
    public String toString() {
        return face + " of " + suit;
    }
}
