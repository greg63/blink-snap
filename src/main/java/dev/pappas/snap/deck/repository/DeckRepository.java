package dev.pappas.snap.deck.repository;

import dev.pappas.snap.deck.domain.CardEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Stack;

@Component
public class DeckRepository {
    private Stack<CardEntity> deck;

    public void createDeck(Stack<CardEntity> deck) {
        this.deck = deck;
    }

    public Optional<CardEntity> fetchNextCard() {
        return deck.isEmpty() ? Optional.empty() : Optional.of(deck.pop());
    }
}
