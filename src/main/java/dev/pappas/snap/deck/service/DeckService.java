package dev.pappas.snap.deck.service;

import dev.pappas.snap.deck.api.Card;
import dev.pappas.snap.deck.api.Face;
import dev.pappas.snap.deck.api.NoCardsLeftException;
import dev.pappas.snap.deck.api.Suit;
import dev.pappas.snap.deck.domain.CardEntity;
import dev.pappas.snap.deck.repository.DeckRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DeckService {

    private final DeckRepository deckRepository;

    public DeckService(DeckRepository deckRepository) {
        this.deckRepository = deckRepository;
    }

    public void createDeck() {
        List<CardEntity> deck = Stream.of(Face.values())
                .flatMap(face -> Stream.of(Suit.values()).map(suit ->
                        new CardEntity(face, suit))
                ).collect(Collectors.toList());

        Collections.shuffle(deck);

        Stack<CardEntity> deckStack = new Stack<>();
        deck.forEach(deckStack::push);
        deckRepository.createDeck(deckStack);
    }

    public Card getNextPlayerCard() throws NoCardsLeftException {
        return deckRepository.fetchNextCard()
                .map(card -> new Card(card.getFace(), card.getSuit()))
                .orElseThrow(NoCardsLeftException::new);
    }
}
