package dev.pappas.snap.deck.domain;

import dev.pappas.snap.deck.api.Face;
import dev.pappas.snap.deck.api.Suit;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CardEntity {
    private Face face;
    private Suit suit;
}
