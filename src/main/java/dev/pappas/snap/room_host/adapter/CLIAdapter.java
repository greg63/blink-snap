package dev.pappas.snap.room_host.adapter;

import dev.pappas.snap.room_host.api.PlayersTurn;
import dev.pappas.snap.room_host.api.EndGame;
import dev.pappas.snap.room_host.api.StartTurn;
import dev.pappas.snap.room_host.service.RoomHostService;
import dev.pappas.snap.utils.InputListener;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.PrintStream;
import java.util.List;

@Component
public class CLIAdapter implements CommandLineRunner {
    private final InputListener inputListener;
    private final PrintStream p;
    private final RoomHostService roomHostService;

    public CLIAdapter(PrintStream printStream,
                      InputListener inputListener,
                      RoomHostService roomHostService) {
        this.p = printStream;
        this.inputListener = inputListener;
        this.roomHostService = roomHostService;
    }

    private void setup() {
        p.println("Hello and welcome to Snap, I am your dealer Jarvis");
        p.println("before we start, I need to know a few things:");
        p.println("What is player One's name?");
        String playerOne = inputListener.getNextString();
        p.println("Thank you, And what is Player Two's Name?");
        String playerTwo = inputListener.getNextString();
        p.printf("Excellent, Lovely to meet you %s and %s, May the best person win!%n", playerOne, playerTwo);
        roomHostService.launchGame(List.of(playerOne, playerTwo));
    }

    @EventListener
    public void startTurn(StartTurn startTurn) {
        p.println("ok it looks like we're ready to Go!");
        p.println("remember it's only suits that count");
        p.println("remember to press:");
        p.println("\t Return if there's no match");
        p.println("\t or the name of the player who shouted snap first!");
        p.println("Current stake size is:" + startTurn.getStake());
        printCardTurn(startTurn.getPlayerOne());
        printCardTurn(startTurn.getPlayerTwo());
        String nextInput = inputListener.getNextString();
        roomHostService.completeTurn(nextInput);
    }

    @EventListener
    public void gameFinished(EndGame endGame) {
        p.println("That's it folks!");
        if (endGame.isHasWinner()) {
            p.println("And the winner is..." + endGame.getWinner().name() + " with a stack height of " + endGame.getWinner().potSize());
            p.println("And the loser is..." + endGame.getLoser() + endGame.getLoser().potSize());
        } else {
            p.println("nobody won!");
        }
    }

    private void printCardTurn(PlayersTurn player) {
        p.println(player.name() + " plays: " + player.card());
    }

    @Override
    public void run(String... args) {
        setup();
    }
}
