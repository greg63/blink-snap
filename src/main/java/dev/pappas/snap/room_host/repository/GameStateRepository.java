package dev.pappas.snap.room_host.repository;

import dev.pappas.snap.room_host.domain.GameStateEntity;
import org.springframework.stereotype.Component;

@Component
public class GameStateRepository {

    private GameStateEntity gameState;

    public GameStateEntity save(GameStateEntity gameState) {
        this.gameState = gameState;
        return this.gameState;
    }

    public GameStateEntity fetch() {
        return gameState;
    }
}
