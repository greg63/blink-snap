package dev.pappas.snap.room_host.api;

import dev.pappas.snap.player.api.Player;
import lombok.Builder;
import lombok.Value;
import org.springframework.context.ApplicationEvent;

@Value
public class EndGame extends ApplicationEvent {
    boolean hasWinner;
    Player winner;
    Player loser;

    @Builder
    public EndGame(Object source, Player winner, Player loser, boolean hasWinner) {
        super(source);
        this.hasWinner = hasWinner;
        this.winner = winner;
        this.loser = loser;
    }
}
