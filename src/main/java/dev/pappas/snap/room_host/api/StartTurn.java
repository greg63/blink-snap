package dev.pappas.snap.room_host.api;

import lombok.Builder;
import lombok.Value;
import org.springframework.context.ApplicationEvent;

@Value
public class StartTurn extends ApplicationEvent {
    int stake;
    PlayersTurn playerOne;
    PlayersTurn playerTwo;

    @Builder
    public StartTurn(Object source, int stake, PlayersTurn playerOne, PlayersTurn playerTwo) {
        super(source);
        this.stake = stake;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }
}
