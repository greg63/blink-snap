package dev.pappas.snap.room_host.api;

import dev.pappas.snap.deck.api.Card;

public record PlayersTurn(String name, Card card) {
}
