package dev.pappas.snap.room_host.domain;

import lombok.Data;

@Data
public class GameStateEntity {
    private int currentPot;

    public void addToPot(int newCards) {
        currentPot += newCards;
    }

    public void flushPot() {
        currentPot = 0;
    }
}
