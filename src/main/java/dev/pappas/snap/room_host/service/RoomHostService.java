package dev.pappas.snap.room_host.service;

import dev.pappas.snap.deck.api.NoCardsLeftException;
import dev.pappas.snap.deck.service.DeckService;
import dev.pappas.snap.player.api.Player;
import dev.pappas.snap.player.service.PlayerService;
import dev.pappas.snap.room_host.api.EndGame;
import dev.pappas.snap.room_host.api.PlayersTurn;
import dev.pappas.snap.room_host.api.StartTurn;
import dev.pappas.snap.room_host.domain.GameStateEntity;
import dev.pappas.snap.room_host.repository.GameStateRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomHostService {
    private final GameStateRepository gameStateRepository;
    private final DeckService deckService;
    private final PlayerService playerService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public RoomHostService(GameStateRepository gameStateRepository, DeckService deckService,
                           PlayerService profileService, ApplicationEventPublisher applicationEventPublisher) {
        this.gameStateRepository = gameStateRepository;
        this.deckService = deckService;
        this.playerService = profileService;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void launchGame(List<String> playerNames) {
        GameStateEntity gameState = gameStateRepository.save(new GameStateEntity());
        deckService.createDeck();
        List<Player> players = playerService.createPlayers(playerNames);
        prepareTurn(players, gameState);
    }

    private void prepareTurn(List<Player> players, GameStateEntity gameState) {
        PlayersTurn playerOne;
        PlayersTurn playerTwo;
        try {
            playerOne = new PlayersTurn(players.get(0).name(), deckService.getNextPlayerCard());
            playerTwo = new PlayersTurn(players.get(1).name(), deckService.getNextPlayerCard());
        } catch (NoCardsLeftException exception) {
            endGame();
            return;
        }
        gameState.addToPot(2);
        gameStateRepository.save(gameState);
        applicationEventPublisher.publishEvent(StartTurn.builder()
                .source(this)
                .playerOne(playerOne)
                .playerTwo(playerTwo)
                .stake(gameState.getCurrentPot())
                .build()
        );
    }

    private void endGame() {
        applicationEventPublisher.publishEvent(
                EndGame.builder()
                        .source(this)
                        .hasWinner(false)
                        .build());
    }

    public void completeTurn(String nextInput) {
        if(nextInput == null|| nextInput.isBlank()) {
            List<Player> players = playerService.fetchPlayers();
            prepareTurn(players, gameStateRepository.fetch());
        }
//        TODO: add a validationService to validate the current state of play
//        TODO: if successful assign the pot to the person who called it.
//        TODO: flush the global pot
    }
}
