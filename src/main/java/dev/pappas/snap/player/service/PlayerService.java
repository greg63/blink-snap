package dev.pappas.snap.player.service;

import dev.pappas.snap.player.api.Player;
import dev.pappas.snap.player.domain.PlayerEntity;
import dev.pappas.snap.player.repository.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PlayerService {

    private final PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public List<Player> createPlayers(List<String> players) {
        return Stream.of(playerRepository.savePlayerOne(new PlayerEntity(players.get(0))),
                       playerRepository.savePlayerTwo(new PlayerEntity(players.get(1))))
                .map(player -> mapToApi(player))
                .collect(Collectors.toList());
    }

    public List<Player> fetchPlayers() {
        return playerRepository.fetchAll().stream().map(player -> mapToApi(player)).collect(Collectors.toList());
    }

    private Player mapToApi(PlayerEntity player) {
        return new Player(player.getPlayerName(), player.getPotSize());
    }


}
