package dev.pappas.snap.player.api;

public record Player(String name, int potSize) {
}
