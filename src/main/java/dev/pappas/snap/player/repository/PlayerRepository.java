package dev.pappas.snap.player.repository;

import dev.pappas.snap.player.domain.PlayerEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlayerRepository {
    private PlayerEntity playerOne;
    private PlayerEntity playerTwo;

    public PlayerEntity savePlayerOne(PlayerEntity playerOne) {
        this.playerOne = playerOne;
        return playerOne;
    }

    public PlayerEntity savePlayerTwo(PlayerEntity playerTwo) {
        this.playerTwo = playerTwo;
        return playerTwo;
    }

    public List<PlayerEntity> fetchAll() {
        return List.of(playerOne, playerTwo);
    }
}
