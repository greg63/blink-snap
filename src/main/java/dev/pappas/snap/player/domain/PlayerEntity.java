package dev.pappas.snap.player.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Setter(AccessLevel.NONE)
public class PlayerEntity {
    private String playerName;
    private int potSize;

    public PlayerEntity(String playerName) {
        this.playerName = playerName;
    }
}
