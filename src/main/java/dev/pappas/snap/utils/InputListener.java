package dev.pappas.snap.utils;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class InputListener {
    private static final Scanner SCANNER = new Scanner(System.in);

    public String getNextString() {
        return SCANNER.nextLine();
    }
}
