package dev.pappas.snap.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.io.PrintStream;

@Configuration
public class SnapConfiguration {
    @Bean
    public PrintStream printStream() {
        return System.out;
    }

    @Bean(name = "applicationEventMulticaster")
    @ConditionalOnProperty(value = "dev.pappas.snap.multi_threaded.enabled", havingValue = "true", matchIfMissing = true)
    public ApplicationEventMulticaster applicationEventMulticaster() {
        SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();
        eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return eventMulticaster;
    }
}
